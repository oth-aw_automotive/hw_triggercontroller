/**
 * MIT License
 *
 * Copyright (c) 2018-2020
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 *  TM1638.h
 *  TM1638 library for stm32f10x
 * 
 *      
 *      
 * 
 *  Command / register definitions
*
*   MSB           LSB
*    7 6 5 4 3 2 1 0
*   -----------------
*    0 1 - - - - - -    Data command
*    1 0 - - - - - -    Display control command
*    1 1 - - - - - -    Address command
*
*
*   7.1 Data Command Set
*
*   MSB           LSB
*    7 6 5 4 3 2 1 0
*    -----------------
*    0 1 0 0 0 - 0 0    Write display data
*    0 1 0 0 0 - 1 0    Read key scan data
*    0 1 0 0 0 0 - -    Auto address increment
*    0 1 0 0 0 1 - -    Fixed address
*
*
*   7.2 Address command set
*
*   MSB           LSB
*    7 6 5 4 3 2 1 0
*    -----------------
*    1 1 0 - A A A A    Address 0x00..0x0F
*
*
*   7.3 Display Control
*
*   MSB           LSB
*    7 6 5 4 3 2 1 0
*   -----------------
*    1 0 0 0 - 0 0 0    Set the pulse width of 1 / 16
*    1 0 0 0 - 0 0 1    Set the pulse width of 2 / 16
*    1 0 0 0 - 0 1 0    Set the pulse width of 4 / 16
*    1 0 0 0 - 0 1 1    Set the pulse width of 10 / 16
*    1 0 0 0 - 1 0 0    Set the pulse width of 11 / 16
*    1 0 0 0 - 1 0 1    Set the pulse width of 12 / 16
*    1 0 0 0 - 1 1 0    Set the pulse width of 13 / 16
*    1 0 0 0 - 1 1 1    Set the pulse width of 14 / 16
*    1 0 0 0 0 - - -    Display off
*    1 0 0 0 1 - - -    Display on
*   
*/



#ifndef TM1638_H
#define TM1638_H

#include "stm32f1xx_hal.h"
#include "tim.h"

// Commands
#define TM1638_CMD_DATA                 0x40 //!< Display data command
#define TM1638_CMD_CTRL                 0x80 //!< Display control command
#define TM1638_CMD_ADDR                 0xc0 //!< Display address command

// Data command bits
#define TM1638_DATA_WRITE	            0x00 //!< Write data
#define TM1638_DATA_READ_KEYS           0x02 //!< Read keys
#define TM1638_DATA_AUTO_INC_ADDR       0x00 //!< Auto increment address
#define TM1638_DATA_FIXED_ADDR	        0x04 //!< Fixed address

// Control command bits
#define TM1638_CTRL_PULSE_1_16          0x00 //!< Pulse width 1/16
#define TM1638_CTRL_PULSE_2_16          0x01 //!< Pulse width 2/16
#define TM1638_CTRL_PULSE_4_16          0x02 //!< Pulse width 4/16
#define TM1638_CTRL_PULSE_10_16         0x03 //!< Pulse width 10/16
#define TM1638_CTRL_PULSE_11_16         0x04 //!< Pulse width 11/16
#define TM1638_CTRL_PULSE_12_16         0x05 //!< Pulse width 12/16
#define TM1638_CTRL_PULSE_13_16         0x06 //!< Pulse width 13/16
#define TM1638_CTRL_PULSE_14_16         0x07 //!< Pulse width 14/16
#define TM1638_CTRL_DISPLAY_OFF         0x00 //!< Display off
#define TM1638_CTRL_DISPLAY_ON          0x08 //!< Display on

#define INPUT 0xF0
#define OUTPUT 0x0F


#define _clkPort GPIOB	//!< Clock port
#define _clkPin GPIO_PIN_14   //!< Clock pin

#define _dioPort GPIOB   //!< Data port
#define _dioPin GPIO_PIN_13  //!< Data pin

#define _stbPort GPIOB   //!< Enable port
#define _stbPin GPIO_PIN_12  //!< Enable pin

//#define _brightness
//#define _display

//!< Pin defines
#define TM1638_NUM_GRIDS                16   //!< Number of grid registers

#define TM1638_CLK_LOW()        { TM1638_pinLow(_clkPort, _clkPin); }     //!< CLK pin low
#define TM1638_CLK_HIGH()       { TM1638_pinHigh(_clkPort, _clkPin); }    //!< CLK pin high
#define TM1638_CLK_INPUT()      { TM1638_pinMode(_clkPort, _clkPin, INPUT); }        //!< CLK pin input
#define TM1638_CLK_OUTPUT()     { TM1638_pinMode(_clkPort, _clkPin, OUTPUT); }       //!< CLK pin output

#define TM1638_DIO_LOW()        { TM1638_pinLow(_dioPort, _dioPin); }     //!< DIO pin low
#define TM1638_DIO_HIGH()       { TM1638_pinHigh(_dioPort, _dioPin); }    //!< DIO pin high
#define TM1638_DIO_INPUT()      { TM1638_pinMode(_dioPort, _dioPin, INPUT); }        //!< DIO pin input
#define TM1638_DIO_OUTPUT()     { TM1638_pinMode(_dioPort, _dioPin, OUTPUT); }       //!< DIO pin output
#define TM1638_DIO_READ()       { TM1638_digitalRead(_dioPort, _dioPin); }           //!< DIO pin read

#define TM1638_STB_LOW()        { TM1638_pinLow(_stbPort, _stbPin); }     //!< STB pin low
#define TM1638_STB_HIGH()       { TM1638_pinHigh(_stbPort, _stbPin); }    //!< STB pin high
#define TM1638_STB_INPUT()      { TM1638_pinMode(_stbPort, _stbPin, INPUT); }        //!< STB pin input
#define TM1638_STB_OUTPUT()     { TM1638_pinMode(_stbPort, _stbPin, OUTPUT); }       //!< STB pin output


#define TM1638_PIN_DELAY()      { delay_us(1); }           //!< Delay between pin changes

#define seg0 0x3F
#define seg1 0x06
#define seg2 0x5B
#define seg3 0x4F
#define seg4 0x66
#define seg5 0x6D
#define seg6 0x7D
#define seg7 0x07
#define seg8 0x7F
#define seg9 0x6F
#define segD 0x80
#define segN 0x00

#define seg_1 0x08
#define seg_2 0x40
#define seg_3 0x01
#define seg_13 0x08
#define seg_23 0x48
#define seg_33 0x49

#define Button0 0x00000001
#define Button1 0x00000100
#define Button2 0x00010000
#define Button3 0x01000000
#define Button4 0x00000010
#define Button5 0x00001000
#define Button6 0x00100000
#define Button7 0x10000000

#define seg_r0 0x00
#define seg_r1 0x08
#define seg_r2 0x10
#define seg_r3 0x20
#define seg_r4 0x01
#define seg_r5 0x04
#define seg_r6 0x02
#define seg_r7 0x40

/*!
 * \brief TM1638 class
 */

/*    
		GPIO_TypeDef _clkPort;    //!< Clock port
    uint16_t _clkPin;    //!< Clock pin
    GPIO_TypeDef _dioPort;    //!< Data port
    uint16_t _dioPin;    //!< Data pin
    GPIO_TypeDef _stbPort;    //!< Enable port
    uint16_t _stbPin;    //!< Enable pin
		uint8_t _brightness;
		uint8_t _display;
*/

		typedef struct{
			GPIO_TypeDef clkPort;
			GPIO_TypeDef dioPort;
			GPIO_TypeDef stbPort;
			uint16_t clkPin;
			uint16_t dioPin;
			uint16_t stbPin;
			uint8_t brightness;
			uint8_t displayOn;
		}TM1638_InitTypedef;

		void TM1638_init(TM1638_InitTypedef TM1638_InitStruct);
		void TM1638_pinLow(GPIO_TypeDef *_Port, uint16_t _Pin);
		void TM1638_pinHigh(GPIO_TypeDef *_Port, uint16_t _Pin);
		void TM1638_pinMode(GPIO_TypeDef *_Port, uint16_t _Pin, uint8_t MODE);
		uint8_t TM1638_digitalRead(GPIO_TypeDef *_Port, uint16_t _Pin);

    void TM1638_begin();
    void TM1638_end();
    void TM1638_displayOn();
    void TM1638_displayOff();
    void TM1638_setBrightness(uint8_t brightness);
    void TM1638_clear();
    void TM1638_writeData(uint8_t address, uint8_t data);
    void TM1638_writeDataBuffer(uint8_t address, const uint8_t *buf, uint8_t len);
    uint32_t TM1638_getKeys();




    void TM1638_writeDisplayControl();
    void TM1638_writeCommand(uint8_t cmd);
    void TM1638_writeByte(uint8_t data);
    uint8_t TM1638_readByte();
		
		uint8_t digit2seg(uint8_t digit);
		void uint4digit2seg(uint8_t * seg, uint16_t number);
		
		void delay_us (uint16_t us);
	
#endif
