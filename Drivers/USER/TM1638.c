/*
 * MIT License
 *
 * Copyright (c) 2018-2020
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*!
 * \file TM1638.c
 * \brief TM1638 library for stm32f10x
 * \details
 *      
 *      
 */

#include <TM1638.h>

/*!
 * \brief TM1638 constructor
 * \details
 *      Constructor with pin arguments: C-D-E (Clock, Data, Enable)
 * \param clkPin TM1638 CLK pin.
 * \param dioPin TM1638 DIO pin.
 * \param stbPin TM1638 STB pin.
 */
 
 /*
    GPIO_TypeDef _clkPort;    //!< Clock port
    uint16_t _clkPin;    //!< Clock pin
    GPIO_TypeDef _dioPort;    //!< Data port
    uint16_t _dioPin;    //!< Data pin
    GPIO_TypeDef _stbPort;    //!< Enable port
    uint16_t _stbPin;    //!< Enable pin
		uint8_t _brightness;
		uint8_t _display;
	*/	
	
	
		uint8_t _brightness;
		uint8_t _display;
	
 void TM1638_init(TM1638_InitTypedef TM1638_InitStruct){

}


void TM1638_pinLow(GPIO_TypeDef *_Port, uint16_t _Pin){
	HAL_GPIO_WritePin(_Port, _Pin, GPIO_PIN_RESET);
}
void TM1638_pinHigh(GPIO_TypeDef *_Port, uint16_t _Pin){
	HAL_GPIO_WritePin(_Port, _Pin, GPIO_PIN_SET);
}

void TM1638_pinMode(GPIO_TypeDef *_Port, uint16_t _Pin, uint8_t MODE){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = _Pin;
	//if(MODE == 0xF0){
	if(MODE == INPUT){
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//	}else if(MODE == 0x0F){
	}else if(MODE == OUTPUT){
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	}
	HAL_GPIO_Init(_Port, &GPIO_InitStruct);
}

uint8_t TM1638_digitalRead(GPIO_TypeDef *_Port, uint16_t _Pin){
	GPIO_PinState PinState = HAL_GPIO_ReadPin(_Port, _Pin);
	return (uint8_t) PinState;
}

/*!
 * \brief Initialize TM1638 controller.
 */
void TM1638_begin()
{
    // Initialize pins
    TM1638_STB_HIGH();
    TM1638_DIO_LOW();
    TM1638_CLK_LOW();

    // Set pin mode
    TM1638_DIO_OUTPUT();
    TM1638_CLK_OUTPUT();
    TM1638_STB_OUTPUT();

    // Write _displayOn and _brightness to display control register
    TM1638_writeDisplayControl();

    // Data write with auto address increment
    TM1638_writeCommand(TM1638_CMD_DATA | TM1638_DATA_WRITE | TM1638_DATA_AUTO_INC_ADDR);
}

/*!
 * \brief Disable pins.
 */
void TM1638_end()
{
    TM1638_DIO_INPUT();
    TM1638_CLK_INPUT();
    TM1638_STB_INPUT();

    TM1638_DIO_LOW();
    TM1638_CLK_LOW();
    TM1638_STB_LOW();
}

/*!
 * \brief Turn Display on.
 */
void TM1638_displayOn()
{
    _display = 8;

    TM1638_writeDisplayControl();
}

/*!
 * \brief Turn display off.
 */
void TM1638_displayOff()
{
    _display = 0;

    TM1638_writeDisplayControl();
}

/*!
 * \brief Set brightness LED's.
 * \param brightness
 *    Display brightness value 0..7
 */
void TM1638_setBrightness(uint8_t brightness)
{
    if (brightness <= 7) {
				extern uint8_t _display;
        _brightness = brightness;

        TM1638_writeDisplayControl();
    }
}

/*!
 * \brief Turn all LED's off.
 */
void TM1638_clear()
{
    TM1638_STB_LOW();
    TM1638_writeByte((uint8_t)(TM1638_CMD_ADDR | 0x00));
    for (uint8_t i = 0; i < TM1638_NUM_GRIDS; i++) {
        TM1638_writeByte(0x00);
    }
    TM1638_STB_HIGH();
}

/*!
 * \brief Write display register
 * \param address Display address 0x00..0x0F
 * \param data Value 0x00..0xFF
 */
void TM1638_writeData(uint8_t address, uint8_t data)
{
    if (address <= TM1638_NUM_GRIDS) {
        // Write byte to display register
        TM1638_STB_LOW();
        TM1638_writeByte((uint8_t)(TM1638_CMD_ADDR | address));
        TM1638_writeByte(data);
        TM1638_STB_HIGH();
    }
}

/*!
 * \brief Write buffer to multiple display registers
 * \details
 *    Write buffer to TM1638 with auto address increment
 * \param address
 *    Display address 0x00..0x0F
 * \param buf
 *    Buffer
 * \param len
 *    Buffer length
 */
void TM1638_writeDataBuffer(uint8_t address, const uint8_t *buf, uint8_t len)
{
    if ((address + len) <= TM1638_NUM_GRIDS) {
        // Write buffer to display registers
        TM1638_STB_LOW();
        TM1638_writeByte((uint8_t)(TM1638_CMD_ADDR | address));
        for (uint8_t i = 0; i < len; i++) {
           TM1638_writeByte(buf[i]);
        }
        TM1638_STB_HIGH();
    }
}

/*!
 * \brief Get key states.
 * \return One or more buttons. One bit per button.
 */
uint32_t TM1638_getKeys()
{
    uint32_t keys = 0;

    // Read 4 Bytes key-scan registers
    TM1638_STB_LOW();
    TM1638_STB_HIGH();
    TM1638_STB_LOW();
    TM1638_writeByte(TM1638_CMD_DATA | TM1638_DATA_READ_KEYS);
    for (uint8_t i = 0; i < 4; i++) {
        keys |= ((uint32_t)TM1638_readByte() << (i * 8));
    }
    TM1638_STB_HIGH();

    return keys;
}

// -------------------------------------------------------------------------------------------------
/*!
 * \brief Write display control register.
 * \details
 *      Set brightness and display on/off
 */
void TM1638_writeDisplayControl()
{
    TM1638_writeCommand((uint8_t)(TM1638_CMD_CTRL | _display | _brightness));
}

/*!
 * \brief Write command to TM1638.
 */
void TM1638_writeCommand(uint8_t cmd)
{
    TM1638_STB_LOW();
    TM1638_writeByte(cmd);
    TM1638_STB_HIGH();
}

/*!
 * \brief Write byte to TM1638.
 * \param data 8-bit value.
 */
void TM1638_writeByte(uint8_t data)
{
    for (uint8_t i = 0; i < 8; i++) {
        TM1638_CLK_LOW();
        TM1638_PIN_DELAY();
        if (data & (1 << i)) {
            TM1638_DIO_HIGH();
        } else {
            TM1638_DIO_LOW();
        }
        TM1638_CLK_HIGH();
        TM1638_PIN_DELAY();
    }
}

/*!
 * \brief Read byte from TM1638.
 * \return 8-bit value.
 */
uint8_t TM1638_readByte()
{
    uint8_t data = 0;

    TM1638_DIO_INPUT();
		uint8_t i = 0;
    for (i = 0; i < 8; i++) {
        TM1638_CLK_LOW();
        TM1638_PIN_DELAY();
				//uint8_t nRet = TM1638_DIO_READ();
				uint8_t nRet = TM1638_digitalRead(_dioPort, _dioPin);
			
        //if (TM1638_DIO_READ()) {
        if (nRet) {
            data |= (1 << i);
        }
        TM1638_CLK_HIGH();
        TM1638_PIN_DELAY();
    }
    TM1638_DIO_OUTPUT();

    return data;
}

void delay_us (uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&htim4,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim4) < us);  // wait for the counter to reach the us input in the parameter
}

uint8_t digit2seg(uint8_t digit){
	//uint8_t seg;
	switch (digit)
  {
  	case 0:
			return seg0;
  		break;
  	case 1:
			return seg1;
  		break;
  	case 2:
			return seg2;
  		break;
  	case 3:
			return seg3;
  		break;
  	case 4:
			return seg4;
  		break;
  	case 5:
			return seg5;
  		break;
  	case 6:
			return seg6;
  		break;
  	case 7:
			return seg7;
  		break;
  	case 8:
			return seg8;
  		break;
  	case 9:
			return seg9;
  		break;
  	case 0x2E:
			return segD;
  		break;
  	default:
			return 0;
  		break;
  }
}

void uint4digit2seg(uint8_t * seg, uint16_t number){
	uint8_t ones, tens, hundreds, thousands;
	uint16_t rest;
	
	thousands = number / 1000;
	rest = number - (thousands * 1000);
	hundreds = rest / 100;
	rest = rest - (hundreds * 100);
	tens = rest / 10;
	ones = rest - (tens * 10);
	if(thousands == 0) seg[0] = segN;
	else seg[0] = digit2seg(thousands);
	if((hundreds == 0)&&(thousands == 0)) seg[1] = segN;
	else seg[1] = digit2seg(hundreds);
	if((tens == 0)&&(hundreds == 0)&&(thousands == 0)) seg[2] = segN;
	else seg[2] = digit2seg(tens);
	seg[3] = digit2seg(ones);
	
}



