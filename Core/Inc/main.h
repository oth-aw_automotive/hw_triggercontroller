/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
//#define OS1_Freq 20
#define OS1_Freq_lo 10
#define OS1_Freq_hi 20
#define OneTurn_Resolution 3600
#define System_Freq 72000000
#define MainDelay 360
#define PulseWidth 36
#define Trigger_OS1_Pin GPIO_PIN_1
#define Trigger_OS1_GPIO_Port GPIOA
#define Encoder_A_Pin GPIO_PIN_6
#define Encoder_A_GPIO_Port GPIOA
#define Encoder_B_Pin GPIO_PIN_7
#define Encoder_B_GPIO_Port GPIOA
#define Trigger_Button_Pin GPIO_PIN_0
#define Trigger_Button_GPIO_Port GPIOB
#define Trigger_Button_EXTI_IRQn EXTI0_IRQn
#define Trigger_Ch1_Pin GPIO_PIN_8
#define Trigger_Ch1_GPIO_Port GPIOA
#define Trigger_Ch2_Pin GPIO_PIN_9
#define Trigger_Ch2_GPIO_Port GPIOA
#define Trigger_Ch3_Pin GPIO_PIN_10
#define Trigger_Ch3_GPIO_Port GPIOA
#define Trigger_Ch4_Pin GPIO_PIN_11
#define Trigger_Ch4_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */
#define Trigger1_On (0*(OneTurn_Resolution/4) + 1)
#define Trigger1_Off (0*(OneTurn_Resolution/4) + PulseWidth + 1)
#define Trigger2_On (1*(OneTurn_Resolution/4) + 1)
#define Trigger2_Off (1*(OneTurn_Resolution/4) + PulseWidth + 1)
#define Trigger3_On (2*(OneTurn_Resolution/4) + 1)
#define Trigger3_Off (2*(OneTurn_Resolution/4) + PulseWidth + 1)
#define Trigger4_On (3*(OneTurn_Resolution/4) + 1)
#define Trigger4_Off (3*(OneTurn_Resolution/4) + PulseWidth + 1)

#define Trigger_Port (GPIOA)
#define Trigger_Ch1 (GPIO_PIN_8)
#define Trigger_Ch2 (GPIO_PIN_9)
#define Trigger_Ch3 (GPIO_PIN_10)
#define Trigger_Ch4 (GPIO_PIN_11)


#define InstructionSizeLimit (20)

//#define Burst_MaxValue 9;
//#define Mode_MaxValue (3)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
