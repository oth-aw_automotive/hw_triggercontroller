/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "delay.h"
#include "TM1638.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
const uint8_t Burst_MaxValue = 4;
const uint8_t Mode_MaxValue = 2;

volatile uint32_t OS1_Freq = OS1_Freq_lo;
volatile uint32_t OS1_Freq_old;
volatile uint32_t Keys;
volatile uint16_t counter_value, counter_oldvalue = 0;
volatile uint8_t burst_value = 1;
volatile uint8_t burst_oldvalue;
volatile uint8_t mode_value = 1;
volatile uint8_t mode_oldvalue;
uint16_t encoderstep = 100*4;
uint16_t encoder_value, encoder_oldvalue = 0;

uint8_t parity = 0;

//volatile uint16_t trigger1_timetab[1+2*Burst_MaxValue];
volatile uint16_t trigger1_timetab[] = {450, 500, 1350, 1400, 2250, 2300, 3150, 3200, 0};
volatile uint16_t trigger2_timetab[] = {550, 600, 1450, 1500, 2350, 2400, 3250, 3300, 0};
volatile uint16_t trigger3_timetab[] = {650, 700, 1550, 1600, 2450, 2500, 3350, 3400, 0};
volatile uint16_t trigger4_timetab[] = {750, 800, 1650, 1700, 2550, 2600, 3450, 3500, 0};

volatile uint16_t testtab[100];
volatile uint16_t* testtab_pointer = testtab;

volatile uint16_t* timetab1_pointer = trigger1_timetab;
volatile uint16_t* timetab2_pointer = trigger2_timetab;
volatile uint16_t* timetab3_pointer = trigger3_timetab;
volatile uint16_t* timetab4_pointer = trigger4_timetab;


unsigned char string[] = "OTH-AW: Trigger Controller v1.1 firmware 20211125beta ";
uint8_t instruction[20];
uint8_t char_counter = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
	HAL_SYSTICK_Config(SystemCoreClock/7200);
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
	//HAL_TIM_Base_Start_IT (& htim1);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_Base_Start (& htim2);
	HAL_TIM_Base_Start (& htim4);
	uint8_t seg[4];	
	TM1638_begin();
	TM1638_clear();
	TM1638_displayOn();
	TIM3->CNT = 1800 << 2;
	//
	
	TM1638_writeData(0x08, (segD));
	HAL_UART_Transmit(&huart1, string, 54, 100);
	//HAL_UART_Receive_IT(&huart1, instruction, 20);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
	uint16_t max;
  while (1)
  {
		Keys = TM1638_getKeys();
		if(Keys != 0){
			switch (Keys)
      {
      	case Button0:
					counter_value = 0;
					parity = 0;
					if(TIM3->CNT <= ((3600<<2) - encoderstep)) TIM3->CNT += encoderstep;
					delay_ms(800);
      		break;
      	case Button1:
					counter_value = 0;
					parity = 0;
					if(TIM3->CNT >= encoderstep) TIM3->CNT -= encoderstep;
					delay_ms(800);
      		break;
      	case Button2:
      		break;
      	case Button3:
      		break;
      	case Button4:
					if(OS1_Freq == OS1_Freq_lo) OS1_Freq = OS1_Freq_hi;
					else OS1_Freq = OS1_Freq_lo;
				  MX_TIM1_Init();
				  MX_TIM2_Init();
				  HAL_TIM_Base_Start (& htim2);
					delay_ms(1000);
      		break;
      	case Button5:		
      		break;
      	case Button6:
					counter_value = 0;
					parity = 0;
					mode_value = 1+ mode_value % Mode_MaxValue;
					delay_ms(1000);
      		break;
      	case Button7:
					counter_value = 0;
					parity = 0;
					burst_value = 1+ burst_value % Burst_MaxValue;
					delay_ms(1000);
      		break;
      	default:
      		break;
      }
		}
		
		encoder_value = TIM3->CNT >> 2;
		if(encoder_value != encoder_oldvalue){		
			uint4digit2seg(seg,  encoder_value);
			TM1638_writeData(0x00, seg[0]);
			TM1638_writeData(0x02, seg[1]);
			TM1638_writeData(0x04, (seg[2] | segD));
			TM1638_writeData(0x06, seg[3]);
			TIM2->ARR = encoder_value;
			encoder_oldvalue = encoder_value;
		}
		
		if(mode_value != mode_oldvalue){
			uint4digit2seg(seg,  mode_value);
			TM1638_writeData(0x0C, (seg[3] | segD));
			mode_oldvalue = mode_value;
			switch (mode_value)
      {
      	case 1:
					TIM1->ARR = trigger1_timetab[7]+1;
					TIM1->CCR1 = trigger1_timetab[0];
				
					timetab1_pointer = trigger1_timetab;
				
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch1, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch2, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch3, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch4, GPIO_PIN_RESET);
					counter_value = 0;
					parity = 0;
				
					HAL_TIM_OC_Stop_IT (& htim1, TIM_CHANNEL_1);
					HAL_TIM_OC_Stop_IT (& htim1, TIM_CHANNEL_2);
					HAL_TIM_OC_Stop_IT (& htim1, TIM_CHANNEL_3);
					HAL_TIM_OC_Stop_IT (& htim1, TIM_CHANNEL_4);
					HAL_TIM_IRQHandler(&htim1);
					HAL_TIM_OC_Start_IT (& htim1, TIM_CHANNEL_1);
					
      		break;
      	case 2:
					max = trigger1_timetab[7];
					if (trigger2_timetab[7] > max) max = trigger2_timetab[7];
					if (trigger3_timetab[7] > max) max = trigger3_timetab[7];
					if (trigger4_timetab[7] > max) max = trigger4_timetab[7];
					TIM1->ARR = max+1;
				
					TIM1->CCR1 = trigger1_timetab[0];
					TIM1->CCR2 = trigger2_timetab[0];
					TIM1->CCR3 = trigger3_timetab[0];
					TIM1->CCR4 = trigger4_timetab[0];
				
					timetab1_pointer = trigger1_timetab;
					timetab2_pointer = trigger2_timetab;
					timetab3_pointer = trigger3_timetab;
					timetab4_pointer = trigger4_timetab;
			
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch1, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch2, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch3, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch4, GPIO_PIN_RESET);
				
					counter_value = 0;
					parity = 0;
				
					HAL_TIM_IRQHandler(&htim1);
					HAL_TIM_OC_Start_IT (& htim1, TIM_CHANNEL_1);
					HAL_TIM_OC_Start_IT (& htim1, TIM_CHANNEL_2);
					HAL_TIM_OC_Start_IT (& htim1, TIM_CHANNEL_3);
					HAL_TIM_OC_Start_IT (& htim1, TIM_CHANNEL_4);
				
      		break;
      	default:
      		break;
      }
		}
		
		if(burst_value != burst_oldvalue){
			uint4digit2seg(seg,  burst_value);
			TM1638_writeData(0x0E, (seg[3] | segD));
			burst_oldvalue = burst_value;
		}
		
		if(counter_value != counter_oldvalue){
			uint4digit2seg(seg,  counter_value);
			TM1638_writeData(0x0A, (seg[3] | segD));
			counter_oldvalue = counter_value;
		}	
		
		if(OS1_Freq != OS1_Freq_old){
			uint4digit2seg(seg,  counter_value);
			if(OS1_Freq == OS1_Freq_lo) TM1638_writeData(0x08, (seg_13 | segD));
			else TM1638_writeData(0x08, (seg_23 | segD));
			OS1_Freq_old = OS1_Freq;
		}	
		
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
