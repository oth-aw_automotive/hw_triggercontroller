/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "delay.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
	uint32_t actualcount = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart1;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line0 interrupt.
  */
void EXTI0_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_IRQn 0 */
	extern uint8_t counter_value;
	counter_value = 0;
	TIM2->CR1 |= TIM_CR1_CEN; 
  /* USER CODE END EXTI0_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  /* USER CODE BEGIN EXTI0_IRQn 1 */

  /* USER CODE END EXTI0_IRQn 1 */
}

/**
  * @brief This function handles TIM1 capture compare interrupt.
  */
void TIM1_CC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_CC_IRQn 0 */
	extern uint8_t mode_value;
	extern uint8_t burst_value;
	extern uint8_t counter_value;
	extern uint8_t parity;
	
	extern uint16_t trigger1_timetab[];
	extern uint16_t trigger2_timetab[];
	extern uint16_t trigger3_timetab[];
	extern uint16_t trigger4_timetab[];
	
	extern uint16_t* timetab1_pointer;
	extern uint16_t* timetab2_pointer;
	extern uint16_t* timetab3_pointer;
	extern uint16_t* timetab4_pointer;
	
	//extern uint16_t* testtab_pointer;
	
	uint8_t interrupt_source = 0;
	actualcount = TIM1->CNT;
	//*testtab_pointer = TIM1->CNT;
	//*testtab_pointer = TIM1->CCR1;
	//testtab_pointer++;
	
	// find interrupt source
	if (TIM1->SR & TIM_SR_CC1IF_Msk){
		interrupt_source = 1;
	}else if (TIM1->SR & TIM_SR_CC2IF_Msk){
		interrupt_source = 2;
	}else if (TIM1->SR & TIM_SR_CC3IF_Msk){
		interrupt_source = 3;
	}else if (TIM1->SR & TIM_SR_CC4IF_Msk){
		interrupt_source = 4;
	}
	
	// increment timetab pointer to the next rdv, output
	if (actualcount == 0){
		counter_value = 0;
		parity = 0;
	}else if(counter_value < burst_value){
	//}else{
		switch (interrupt_source){
			case 1:
				//if(actualcount == *timetab1_pointer){
					// output
					if(mode_value == 2){
						HAL_GPIO_TogglePin(Trigger_Port, Trigger_Ch1);
					}else if (mode_value == 1){
						HAL_GPIO_TogglePin(Trigger_Port, Trigger_Ch1 | Trigger_Ch2 | Trigger_Ch3 | Trigger_Ch4);
						// update couter_value
						parity = ~parity;
						if (parity == 0) counter_value++;
					}
//					*testtab_pointer = counter_value;
//					testtab_pointer++;
					// increment timetab pointer to to load the next timespot
					timetab1_pointer++;
					if(*timetab1_pointer == 0) timetab1_pointer = trigger1_timetab;
					TIM1->CCR1 = *timetab1_pointer;
				//}
				break;
			case 2:
				//*testtab_pointer = TIM1->CNT;
				//testtab_pointer++;
				// output
				HAL_GPIO_TogglePin(Trigger_Port, Trigger_Ch2);
				// increment timetab pointer to to load the next timespot
				timetab2_pointer++;
				if(*timetab2_pointer == 0) timetab2_pointer = trigger2_timetab;
				TIM1->CCR2 = *timetab2_pointer;
				break;
			case 3:
				// output
				HAL_GPIO_TogglePin(Trigger_Port, Trigger_Ch3);
				// increment timetab pointer to to load the next timespot
				timetab3_pointer++;
				if(*timetab3_pointer == 0) timetab3_pointer = trigger3_timetab;
				TIM1->CCR3 = *timetab3_pointer;
				break;
			case 4:
				// output
				HAL_GPIO_TogglePin(Trigger_Port, Trigger_Ch4);
				// increment timetab pointer to to load the next timespot
				timetab4_pointer++;
				if(*timetab4_pointer == 0) timetab4_pointer = trigger4_timetab;
				TIM1->CCR4 = *timetab4_pointer;
				// update couter_value
				parity = ~parity;
				if (parity == 0) counter_value++;
				break;
			default:
				break;
			}
		
		}
		if(counter_value >= burst_value){
			timetab1_pointer = trigger1_timetab;
			timetab2_pointer = trigger2_timetab;
			timetab3_pointer = trigger3_timetab;
			timetab4_pointer = trigger4_timetab;
			
			TIM1->CCR1 = trigger1_timetab[0];
			TIM1->CCR2 = trigger2_timetab[0];
			TIM1->CCR3 = trigger3_timetab[0];
			TIM1->CCR4 = trigger4_timetab[0];
			
			HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch1, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch2, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch3, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch4, GPIO_PIN_RESET);
			
			counter_value = 0;
			parity = 0;
			
		
		}	
	// output
	/*
	if (actualcount == Trigger1_On ){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch1, GPIO_PIN_SET);
		TIM1->CCR1 = Trigger1_Off;
	}else if (actualcount == Trigger1_Off){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch1, GPIO_PIN_RESET);
		TIM1->CCR1 = Trigger2_On;
	}else if (actualcount == Trigger2_On){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch2, GPIO_PIN_SET);
		TIM1->CCR1 = Trigger2_Off;
	}else if (actualcount == Trigger2_Off){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch2, GPIO_PIN_RESET);
		TIM1->CCR1 = Trigger3_On;
	}else if (actualcount == Trigger3_On){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch3, GPIO_PIN_SET);
		TIM1->CCR1 = Trigger3_Off;
	}else if (actualcount == Trigger3_Off){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch3, GPIO_PIN_RESET);
		TIM1->CCR1 = Trigger4_On;
	}else if (actualcount == Trigger4_On){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch4, GPIO_PIN_SET);
		TIM1->CCR1 = Trigger4_Off;
	}else if (actualcount == Trigger4_Off){
		HAL_GPIO_WritePin(Trigger_Port, Trigger_Ch4, GPIO_PIN_RESET);
		TIM1->CCR1 = Trigger1_On;
	}
	*/
  /* USER CODE END TIM1_CC_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_CC_IRQn 1 */

  /* USER CODE END TIM1_CC_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
	extern uint8_t char_counter;
	extern uint8_t instruction;
	
	char_counter++;
	if (char_counter > InstructionSizeLimit){
		char_counter = 0;
	}
  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */

  /* USER CODE END USART1_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
