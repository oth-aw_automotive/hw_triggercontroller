/**
  ******************************************************************************
  * @file           : delay.c
  * @brief          : delay in ms
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */
#include "delay.h"

inline uint32_t millis (void)
{
   return uwTick;
}
void delay_ms (uint32_t t)
{
  uint32_t start, end;
  start = millis();
  end = start + t;
  if (start < end) { 
  	while ((millis() >= start) && (millis() < end)) { 
  	  // do nothing 
  	} 
  } else { 
    while ((millis() >= start) || (millis() < end)) {
      // do nothing
    };
  }
}
